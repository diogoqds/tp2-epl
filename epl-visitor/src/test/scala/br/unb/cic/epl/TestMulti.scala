package br.unb.cic.epl

import org.scalatest.FlatSpec
import org.scalatest.Matchers
import org.scalatest.GivenWhenThen
import org.scalatest.BeforeAndAfter


class TestMult extends FlatSpec with Matchers with GivenWhenThen with BeforeAndAfter {

  behavior of "An Mult expression"

  var literal8: Literal = _
  var literal6: Literal = _

  before {
    literal8 = new Literal(8) 
    literal6 = new Literal(6) 
  }

  it should "return String (8 * 6) when we call Mult(Literal(8), Literal(6).print())" in {
    val mult = new Mult(literal8, literal6)
  
    mult.print() should be ("(8 * 6)")
  }

  it should "return 48 when we call Sub(Literal(8), Literal(6)).eval()" in {
    val eval = new Eval()
    val mult = new Mult(literal8, literal6)

    mult.accept(eval)

    eval.result() should be (48)
  }
}
