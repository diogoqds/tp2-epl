package br.unb.cic.epl

object Main extends App {
  var lit50 = new Literal(50)
  var lit30 = new Literal(30)
  
  val add = new Add(lit30,lit50)
  val sub = new Sub(lit30,lit50)
  val mult = new Mult(lit30,lit50)
  
  val evalAdd = new Eval()
  val evalSub = new Eval()
  val evalMult = new Eval()

  add.accept(evalAdd)
  sub.accept(evalSub)
  mult.accept(evalMult)

  println(add.print())
  println(evalAdd.result())
  
  println(sub.print())
  println(evalSub.result())
  
  println(mult.print())
  println(evalMult.result())
}